#!/usr/bin/env python
'''
(C) 2018 - 2019 by Matteo Pasotti <matteo.pasotti@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import re
import requests
import os
import calendar
import time

SCROLL_PAUSE_TIME=1.3
username = ""
password = ""

url = 'https://www.kindertap.com/'

driver = webdriver.Chrome(executable_path=r"/usr/bin/chromedriver")
driver.implicitly_wait(15)
driver.get(url + "/login")

ipt_username = driver.find_element_by_id('username')
ipt_username.send_keys(username)
print("[*] Populating username")
ipt_password = driver.find_element_by_id('password')
ipt_password.send_keys(password)
print("[*] Populating password")

ipt_loginbtn = driver.find_element_by_id('loginButton')
ipt_loginbtn.click()
print("[*] Doing login")

element = WebDriverWait(driver, 60).until(
        EC.presence_of_element_located(
            (
                By.XPATH,
                "//a[contains(@class,'al-sidebar-list-link') and contains(@href, '#/pages/media')]"
                )
            )
        )

print("[*] Moving to media page")
driver.get(url + "/parent/#/pages/media")
div = WebDriverWait(driver, 60).until(
        EC.presence_of_element_located((By.CSS_SELECTOR,"div.datesBox"))
        )

cookies_list = driver.get_cookies()
cookies_dict = {}
for cookie in cookies_list:
    cookies_dict[cookie['name']] = cookie['value']
cookies=cookies_dict

headers = {'User-Agent': driver.execute_script("return navigator.userAgent;")}

ktdtpicker_dal = driver.find_element_by_xpath("//div[@class='datesBox']/kt-date-time-picker[1]/a[@class='currentValueBox dropdown-toggle']/span")

driver.implicitly_wait(20)

if ktdtpicker_dal:
    print("[*] Found ktdtpicker")
    for y in range(2018,2019):
        for m in range(1,13):
            print("[*] Mese in scarico: %s" % m)
            last_height = 0
            new_height = 0
            giornoinizio = 1
            giornofine = calendar.monthrange(y,m)[1]
            print("[*] Giorni mese: %s - %s" % (giornoinizio , giornofine))
            if(giornoinizio < 10):
                sgi = "0%d" % giornoinizio
            else:
                sgi = "%d" % giornoinizio
            print("[*] SGI %s" % sgi)
        
            print("[*] Injecting date inside first kt-date-time-picker")
            driver.execute_script("$('div.datesBox > kt-date-time-picker > a.currentValueBox.dropdown-toggle:eq(0)').click()")
            driver.execute_script("$('div.datesBox > kt-date-time-picker:eq(0) > a > span').text('%s/%s/%s');" % (sgi, m, y))
            # driver.execute_script("$('div.datesBox > kt-calendar:eq(0)').trigger('changeSelection');")
        
            driver.implicitly_wait(30)
        
            print("[*] Injecting date inside second kt-date-time-picker")
            driver.execute_script("$('div.datesBox > kt-date-time-picker > a.currentValueBox.dropdown-toggle:eq(1)').click()")
            driver.execute_script("$('div.datesBox > kt-date-time-picker:eq(1) > a > span').text('%s/%s/%s');" % (giornofine, m, y))
            # driver.execute_script("$('div.datesBox > kt-calendar:eq(1)').trigger('changeSelection');")
        
            driver.implicitly_wait(30)
            time.sleep(SCROLL_PAUSE_TIME)       
        
            # driver.execute_script("$('button.btn.btn-secondary.ml-3').click();")    
            # driver.implicitly_wait(40)
            # time.sleep(SCROLL_PAUSE_TIME)
        
            last_height = driver.execute_script("return document.body.scrollHeight")
        
            while True:
                driver.execute_script("window.scrollTo(0,document.body.scrollHeight);")
                time.sleep(SCROLL_PAUSE_TIME)
                new_height = driver.execute_script("return document.body.scrollHeight;")
                print("[*] Last Height: %s - scrolling" % last_height)
                if new_height == last_height:
                    break
                last_height = new_height
        
            driver.implicitly_wait(60)

            pattern = re.compile("^background\-image\:")
            filenamepattern = re.compile("url\(\"(.+)\"\);")
            for i in driver.find_elements_by_xpath("//div[@class='media']"):
                if(pattern.match(i.get_attribute('style'))):
                    matches = filenamepattern.search(i.get_attribute('style'))
                if matches:
                    remotepath = matches.group(1)
                    #print("[*] Match: %s" % remotepath)
                    # ActionChains(driver).key_down(Keys.CONTROL).send_keys('t').key_up(Keys.CONTROL).perform()
                    dest_file = os.path.join('dl/',re.sub(r'\.jpg\?.*$','.jpg',os.path.basename(remotepath)))
                    print("[*] Dest File: %s" % dest_file)
                    if(not os.path.isfile(dest_file)):
                        response = requests.get(remotepath, cookies=cookies, headers=headers)
                        try:
                            response.raise_for_status()
                            with open(dest_file,"wb") as fimg:
                                for block in response.iter_content(1024):
                                    fimg.write(block)
                        except Exception as ex:
                            print(ex)
                    else:
                        print("[*] File already downloaded")
                else:
                    raise Exception('nothing matches')
else:
    print("[*] kt-date-time-picker not found")

####################################
# download diary
####################################
print("[*] Moving to diary page")
driver.get(url + "/parent/#/pages/diary")
div = WebDriverWait(driver, 60).until(
        EC.presence_of_element_located((By.CSS_SELECTOR,"div.dateBox"))
        )
ktdtpicker_dal = driver.find_element_by_xpath("//div[@class='dateBox']/kt-date-time-picker[1]/a[@class='currentValueBox dropdown-toggle']/span")

driver.implicitly_wait(20)

if ktdtpicker_dal:
    print("[*] Found ktdtpicker")
    for y in range(2018,2019):
        for m in range(1,13):
            giornoinizio = 1
            giornofine = calendar.monthrange(y,m)[1]
            for d in range(giornoinizio,giornofine):
                print("[*] Injecting date inside first kt-date-time-picker")
                driver.execute_script("$('div.dateBox > kt-date-time-picker > a.currentValueBox.dropdown-toggle:eq(0)').click()")
                driver.execute_script("$('div.dateBox > kt-date-time-picker:eq(0) > a > span').text('%s/%s/%s');" % (d, m, y))
        
                driver.implicitly_wait(30)
                while True:
                    driver.execute_script("window.scrollTo(0,document.body.scrollHeight);")
                    time.sleep(SCROLL_PAUSE_TIME)
                    new_height = driver.execute_script("return document.body.scrollHeight;")
                    print("[*] Last Height: %s - scrolling" % last_height)
                    if new_height == last_height:
                        break
                    last_height = new_height
                postsbox = driver.find_element_by_xpath("//div[@class='posts-box']")
                with open(os.path.join("dl","%s_%s_%s.txt" % (y,m,d)), "w") as ftxt:
                    ftxt.write(driver.execute_script("return arguments[0].innerHtml;", postsbox))
        



driver.quit()

#vi: tabstop=4 expandtab
