# kindertapper

A small utility made to automate the download of media from kindertapp. 

Dependencies:
-------------

* For Debian users
   * python-requests
   * python-selenium
   * iridium-chromedriver

MADE FOR DIDACTIC PURPOSE, THE AUTHOR CAN'T BE CONSIDERED RESPONSIBLE FOR ANY IMPROPER USE
